import request from 'supertest';
import express from 'express';
import { router } from '../routes/game.mjs';
import GameModel from '../models/gameModel.js';
import mockingoose from 'mockingoose';

const app = new express();
app.use('/', router);

/* The following tests use lambda functions to mock the functionality
   of the mongoose find method (no db connection!). Whatever the mock returns
   will be sent as a JSON response from the server to the client. */

describe('Valid Game Routes', () => {
    test('games all route', async () => {
        const mockData = [
            {
                name: "Rust"
            },
            {
                name: "Warframe"
            },
            {
                name: "Resident Evil 3"
            }
        ];

        const finderMock = query => {
            const whereClause = query.getFilter();
            // 'where' clause of query should be empty (we want all games)
            expect(Object.keys(whereClause).length).toBe(0);

            // version key is the only field that should be exempted from the result
            expect(query.projection().__v).toBeFalsy();

            return mockData;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        const res = await request(app).get('/all');
        expect(res.body.length).toBe(3);
        expect(res.body[0].name).toBe('Rust');
    });

    test('games count route', async () => {
        const countMock = query => {
            // no query is created because find method is not called
            const queryObj = query.getFilter();
            expect(Object.keys(queryObj).length).toBe(0);

            return 1000;
        };

        // makes sure that count function is called
        mockingoose(GameModel).toReturn(countMock, 'count');

        const res = await request(app).get('/count');
        expect(res.statusCode).toBe(200);
        // response should contain a json with a count property
        expect(res.type).toMatch('json');
        expect(res.body.count).toBe(1000);
    });

    test('games index route', async () => {
        const mockGames = [
            {
                name: 'Farming Simulator 19'
            },
            {
                name: 'NBA 2K21'
            }
        ];

        const finderMock = query => {
            // get extra query options provided to the find method
            const queryOptions = query.getOptions();
            // skip first 10 games (given in the url)
            expect(queryOptions.skip).toBe(10);
            // maximum of 120 games to be returned 
            expect(queryOptions.limit).toBe(120);

            return mockGames;
        };

        mockingoose(GameModel).toReturn(finderMock, 'find');

        // valid integer
        const res = await request(app).get('/10');
        expect(res.statusCode).toBe(200);
        expect(res.type).toMatch('json');
    });

    // test('games image route', async () => {
    //     const finderMock = query => {
    //         console.log('GOOD');
    //         return {'img_url': 'url'};
    //     };

    //     mockingoose(GameModel).toReturn(finderMock, 'findOne');

    //     const res = await request(app).get('/123/image');

    // });

});

describe('Invalid Game Routes', () => {
    test('misspelled route', async () => {
        const res = await request(app).get('/alll');
        expect(res.statusCode).toBe(404);
    });

    test('games index route with negative integer', async () => {
        const res = await request(app).get('/-1');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Index must be a positive integer!');
    });

    test('games index route with decimal number', async () => {
        const res = await request(app).get('/7.5');
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Index must be a positive integer!');
    });

    test('games index route with invalid number', async () => {
        // parsing hello to an int will fail and a 404 code will be returned
        const res = await request(app).get('/hello');
        expect(res.statusCode).toBe(404);
    });

    test('games image route with invalid id', async () => {
        const res = await request(app).get('/hello/image');
        // casting to Object Id will fail
        expect(res.statusCode).toBe(404);
        expect(res.text).toBe('Invalid game id!');
    });

});