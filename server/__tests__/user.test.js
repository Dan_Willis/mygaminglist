import request from 'supertest';
import express from 'express';
import { router } from '../routes/user.mjs';
import GameModel from '../models/gameModel.js';
import UserModel from '../models/userModel.js';
import mockingoose from 'mockingoose';

const app = new express();
app.use('/', router);

/**
 * Test suit for all valid user routes. Database will be mocked so that there is no actual DB connection.
 */
describe('Valid User Routes', () => {
  test('get all users route', async () => {
    const mockData = [
      {
          username: "Alex",
          email: "alex@gmail.com"
      },
      {
          username: "John",
          email: "john@gmail.com"
      },
      {
          username: "Matt",
          email: "matt@gmail.com"
      }
    ]

    const finderMock = query => {
      const whereClause = query.getFilter();
      // 'where' clause of query should be empty (we want all users)
      expect(Object.keys(whereClause).length).toBe(0);

      // version key is the only field that should be exempted from the result
      expect(query.projection().__v).toBeFalsy();

      return mockData;
    }

  mockingoose(UserModel).toReturn(finderMock, 'find');

  const res = await request(app).get('/all');
  expect(res.statusCode).toBe(200);
  expect(res.body.length).toBe(3);
  expect(res.body[0].username).toBe('Alex');

  })

  test('get user by email route', async () => {
    const mockData = [{
          username: "John",
          email: "john@gmail.com"
      }]

    const finderMock = query => {
       // access query object passed to mongoose find method 
       const queryKeyword = query.getFilter().email;
       expect(queryKeyword).toBe('john@gmail.com');
       return mockData;
    }

    mockingoose(UserModel).toReturn(finderMock, 'findOne');

    const res = await request(app).get('/john@gmail.com')
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(1);
    expect(res.body[0].username).toBe('John');
  })

  test('get user by id route', async () => {
    const mockData = [{
          _id: 'djsjfladjfksjfkjdfksjfdjskf',
          username: "John",
          email: "john@gmail.com"
      }]

    const finderMock = query => {
       // access query object passed to mongoose find method 
       const queryKeyword = query.getFilter()._id;
      //  console.log(query.getFilter())
       expect(queryKeyword).toBe('djsjfladjfksjfkjdfksjfdjskf');
       return mockData;
    }

    mockingoose(UserModel).toReturn(finderMock, 'findOne');

    const res = await request(app).get('/id/djsjfladjfksjfkjdfksjfdjskf')
    expect(res.statusCode).toBe(200);
    expect(res.body.length).toBe(1);
    expect(res.body[0].username).toBe('John');
  })

  test('get user by username route', async () => {

  })

  test('post route for user creation', async () => {
    // const mockData = {
    //   username: "John",
    //   profile: 'https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1598556351',
    //   email: "john@gmail.com"
    // }

  //   const finderMock = query => {
  //     // access query object passed to mongoose find method 
  //     const whereClause = query.getFilter();
  //     expect(Object.keys(whereClause).length).toBe(0);
  //     return [];
  //  }

    // mockingoose(UserModel).toReturn(finderMock, 'find');

  //   const res = await request(app).post('/login').send({username:"username"})
  //   expect(res.statusCode).toBe(202);
  //   expect(res.body.length).toBe(1);
  //   expect(res.body[0].profile).toBe('https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1598556351');
  })

  test('post route to edit user info', async () => {
  //   const mockData = {
  //     username: "John",
  //     profile: 'https://steamcdn-a.akamaihd.net/steam/apps/945360/header.jpg?t=1598556351',
  //     email: "john@gmail.com"
  //   }

  //   const finderMock = query => {
  //     // access query object passed to mongoose find method 
  //     const whereClause = query.getFilter();
  //     expect(Object.keys(whereClause).length).toBe(0);
  //     return mockData;
  //  }
  //   mockingoose(UserModel).toReturn(finderMock, 'findOneAndUpdate');
  //   const res = await request(app).post('/edit').send(
  //     {files: {file : { name : "name"}},
  //     body: {profile: "hahahhahah"}
  //   })
  //   expect(res.statusCode).toBe(200);
  })

  test('delete user route', async () => {

  })
})

/**
 * Test suit for helper functions used by the routes. Database will be mocked so that there is no actual DB connection.
 */
describe('test helper functions', () => {
  test('deletion of user comments', async () => {

  })

  test('deletion of user likes', async () => {

  })

})

/**
 * Test suit for all invalid user routes. 
 */
describe('test invalid routes', () => {
  test('route does not exist', async () => {
    // const res = await request(app).get('/alll');
    // expect(res.statusCode).toBe(404);
  })

  test('get route with invalid email', async () => {
    
  })

  test('get route with invalid id', async () => {
    // const res = await request(app).get('/id/sfdjjekkejdjk')
    // expect(res.statusCode).toBe(404);
    // expect(res.text).toBe('Invalid user id!');
  })

  test('get route with invalid username', async () => {
    
  })

  test('delete route with invalid email', async () => {

  })
})