import express from "express";
import UserModel from "../models/userModel.js";
import GameModel from '../models/gameModel.js'
import fileUpload from "express-fileupload";
import { BlobServiceClient } from "@azure/storage-blob";
import * as dotenv from "dotenv";
dotenv.config();

// Setting up the Blob Storage Client
const sasToken = process.env.AZURE_SAS;
const containerName = process.env.CONTAINER_NAME;
const storageAccountName = process.env.STORAGE_NAME;
const blobService = new BlobServiceClient(
  `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`
);
const containerClient = blobService.getContainerClient(containerName);

let router = express.Router();
router.use(fileUpload({ createParentPath: true }));


/**
 * @swagger
 * /users/all:
 *  get:
 *    summary: Retrieves all Users
 *    description: A query is made to a database to retrieve all users   
 *    responses:
 *      200:
 *        description: An array of JSON Users is sent
 *      500:
 *        description: The error occured is sent back as a json message
 */
router.get("/all", async (req, res) => {
  try {
    const users = await UserModel.find().select({ __v: 0 });
    res.status(200).json(users);
  } catch (error) {
    console.log("Failed to get all users");
    res.status(500).json({ message: error.message });
  }
});

/**
 * @swagger
 * /users/{email}:
 *   get:
 *     summary: Retrieves a User with a given email
 *     description: Mongoose Query will find the user with the matching email
 */
router.get("/:email", async (req, res) => {
  const user = await UserModel.findOne({ email: req.params.email });
  res.status(200).json(user);
});

// Get a specific user based on id, and return it if it exists
router.get("/id/:id", async (req, res) => {
  try {
    const user = await UserModel.findOne({ _id: req.params.id });
    res.status(200).json(user);
  }
  catch {
    // mongo throws an error if the ObjectId is invalid
    res.status(404).send('Invalid user id!');
  }
});


/**
 * @swagger
 * /users/name/{username}:
 *  get:
 *    summary: Retrieves Users based on a given username
 *    description: A query is made to a database to retrieve the specified users
 */
router.get('/name/:username', async (req, res) => {
  // decode username from URI before using it in a regex query 
  let parsedUsername = decodeURI(req.params.username);

  // escape all regex characters (see filter.mjs for more details)
  parsedUsername = parsedUsername.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

  const user = await UserModel.find({ username: { $regex: parsedUsername, $options: 'i' } })
    .limit(7);
  res.json(user);
});

/** The following route will create a user object if it doesn't exist already
If it exists, then we do change nothing
For both we return the profile image*/

/**
 * @swagger
 * /users/login:
 *  post:
 *    summary: Logs a user onto the website
 *    description: A user will be logged into the website, and in the even he doesn't already have an account, one will be created for him
 *    responses:
 *       200:
 *         description: Good request
 */
router.post("/login", async (req, res) => {
  const user = await UserModel.find({ email: req.body.email });
  if (user.length == 0) {
    const userModel = new UserModel({
      username: req.body.username,
      bio: "No Bio",
      pfp: req.body.profile,
      email: req.body.email,
      bookmarks: [],
      playing: [],
      completed: [],
      likes: [],
      access: 1,
      favorite: null,
    });
    try {
      userModel.save();
    } catch (err) {
      console.log(err);
    }
    res.status(202).json({ profile: req.body.profile });
  } else {
    res.status(202).json({ profile: user[0].pfp });
  }
});

/**
 * @swagger
 * /users/edit:
 *  post:
 *    summary: Edit the information of a corresponding User
 *    description: We will update a given user's properties with the new inputs
 */
router.post("/edit", async (req, res) => {
  let profile;

  // Try catch for blob storage but also to set the correct profile in case user changed it
  try {
    const blobName = req.files.file.name;
    const file = req.files.file;
    const blobPublicUrl = `https://${storageAccountName}.blob.core.windows.net/${containerName}/`;
    const blobClient = containerClient.getBlockBlobClient(blobName);
    const options = { blobHTTPHeaders: { blobContentType: file.mimetype } };
    await blobClient.uploadData(file.data, options);
    profile = blobPublicUrl + encodeURIComponent(blobName);
  } catch (e) {
    profile = req.body.profile;
    console.log("There was no new profile selected");
  }

  // We update the user object based on form data and then send the updated object back
  const update = {
    username: req.body.name,
    email: req.body.email,
    bio: req.body.bio,
    pfp: profile,
  };
  const doc = await UserModel.findOneAndUpdate(
    { email: req.body.email },
    update,
    { new: true }
  );
  res.status(200).json(doc);
});

/**
 * @swagger
 * /users/delete/{email}:
 *  delete:
 *    summary: Delete a User
 *    description: Based on a given email, a User will be deleted from the site 
 */
router.delete("/delete/:email", async (req, res) => {
  const user = await UserModel.findOne({ email: req.params.email });
  deleteUserCommments(user);
  deleteUserLikes(user);
  await UserModel.findOneAndDelete({ email: req.params.email });
  res.send('User successfully deleted.');
})

// This function will take a user and go through all the games he commented on, and then delete those comments
async function deleteUserCommments(user) {
  const comments = [...user.comments]
  const user_id = user._id
  for (const id of comments) {
    const game = await GameModel.findOne({ _id: id })
    let resultArr = game.comments.filter(comment => comment.user.toString() !== user_id.toString())
    let update = { comments: resultArr }
    await GameModel.findOneAndUpdate({ _id: id }, update)
  }
}

// This function will take a user and go through all the games he liked, and then reduce those likes by 1
async function deleteUserLikes(user) {
  const likes = [...user.likes]
  for (const id of likes) {
    const game = await GameModel.findOne({ _id: id });
    let likeCount = --game.likes;
    let update = { likes: likeCount }
    await GameModel.findOneAndUpdate({ _id: id }, update)
  }
}

export { router };
