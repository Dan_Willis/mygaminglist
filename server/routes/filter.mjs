import express from "express";
import GameModel from "../models/gameModel.js";

const router = express.Router();

// filtering routes for games

router.use(function (req, res, next) {
  try {
    // determines if url is decodable (prevents decoding errors for keyword route)
    decodeURIComponent(req.url);
    next();
  } catch {
    res.status(500).send("URL cannot be decoded!");
  }
});

// get games by keyword, case insensitive search. The route treats
// regex characters as string literals to ensure that the user does not
// try to inject any regex expressions inside the search bar.
router.get("/keyword/:keyword", async (req, res) => {
  // decode keyword from URI before using it in a regex query
  let parsedKeyword = decodeURI(req.params.keyword);

  // find regex characters and if they are present, insert double blackshashes before them
  // ($& option allows the matched regex characters to be kept in the string -> without it, they would be removed)
  parsedKeyword = parsedKeyword.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

  const games = await GameModel.find({
    name: { $regex: parsedKeyword, $options: "i" },
  }).limit(7); // first 7 games will be returned (performance reasons)
  res.json(games);
});

// Helper method to format categories (used by category routes)
function getFormattedCategories(categoryStr) {
  // split categories on '/'
  const categoryArr = categoryStr.split("/");
  // convert category array to be init-cap
  const newCategoryArr = categoryArr.map(
    (category) => category[0].toUpperCase() + category.slice(1)
  );
  return newCategoryArr;
}

// get count of games that match the categories provided in the url
router.get("/category/count/*", async (req, res) => {
  try {
    const categoryArr = getFormattedCategories(req.params[0]);
    // $all property requires that all categories in finalArr are matched by the game
    const categoryGamesCount = await GameModel.count({
      tags: { $all: categoryArr },
    });
    res.json({ count: categoryGamesCount });
  } catch {
    res.status(500).send("Invalid categories!");
  }
});

// get categorized games starting from given index
router.get("/category/index/:index/*", async (req, res) => {
  try {
    // return maximum of 120 games (for pagination)
    const GAME_LIMIT = 120;
    // parse given index
    const upperRange = parseInt(req.params.index);

    // parsing failed or index is negative --> don't query the db
    if (upperRange != req.params.index || upperRange < 0) {
      res.status(404).send("Invalid game index!");
    } else {
      const categoryArr = getFormattedCategories(req.params[0]);
      const gamesInRange = await GameModel.find({ tags: { $all: categoryArr } })
        .skip(upperRange)
        .limit(GAME_LIMIT);
      res.json(gamesInRange);
    }
  } catch (err) {
    res.status(500).send("No categories provided!");
  }
});

export { router };
