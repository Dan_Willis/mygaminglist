import express from "express"
import path from "path"
/**
 * When refreshing the webpage, the server will serve the associated html based on the react router.
 */
const __dirname = path.resolve()
const router = express.Router()

router.use(express.json())

router.get(
    [ "/game/*", "/users", "/users/profile/*"],
    (req, res) => {
        res.sendFile(path.join(__dirname, "./client/build/index.html"))
    }
)

router.use((req, res) => {
    res.status(404).send("Something went wrong");
})

export { router }