import express from "express";
import GameModel from "../models/gameModel.js";
import UserModel from "../models/userModel.js";
import fileUpload from "express-fileupload";
import * as filter from "./filter.mjs";

const router = express.Router();
router.use(fileUpload({ createParentPath: true }));
router.use("/filter", filter.router);

// get all games (for homepage)
/**
 * @swagger
 * /games/all:
 *  get:
 *    summary: Retrieve all Games
 *    description: A query is sent to retrieve every single game in the database
 *    responses:
 *      200:
 *        description: An array of JSON Games is sent
 *      500:
 *        description: The error occured is sent back as a json message
 */
router.get("/all", async (req, res) => {
  const games = await GameModel.find().select({ __v: 0 });
  res.json(games);
});

// get count of games (for pagination purposes)
/**
 * @swagger
 * /games/count:
 *  get:
 *    summary: Retrieve the number of Games
 *    description: A query is done to count the number of games in the DB
 */
router.get("/count", async (req, res) => {
  // returns 0 if db is empty
  const count = await GameModel.count();
  res.json({ count: count });
});

/**
 * @swagger
 * /games/{index}:
 *  get:
 *    summary: Retrieve n number of Games
 *    description: Starting from the upper index given, n number of games is sent back
 */
router.get("/:index", async (req, res) => {
  try {
    const GAME_LIMIT = 120;
    // parse given index
    const upperRange = parseInt(req.params.index);
    // parsing failed
    if (isNaN(upperRange)) {
      res.status(404).send("Invalid game index!");
    } else {
      const gamesInRange = await GameModel.find()
        .skip(upperRange)
        .limit(GAME_LIMIT);
      res.json(gamesInRange);
    }
  } catch {
    res.status(404).send("Index must be a positive integer!");
  }
});

/**
 * @swagger
 * /games/{game}/bookmark:
 *  put:
 *    summary: Modify the 'bookmark' status of the selected game
 *    description: An update is made on the 'bookmark' status, by calling helper functions, for the given user and game
 */
router.put("/:game/bookmark", async (req, res) => {
  updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// This helper function will take in an email for the user affected, the game being updated and the category that is clicked
// Based on those 3 variables a different update will occur on the user object
async function updateObj(category, email, game) {
  let user = await UserModel.findOne({ email: email });
  let copy; // Copy of the array we want to updated
  let update; // Value of the update happening
  let other; // Other array that might be affected
  let i; // index value
  // This switch case addresses all possible categories a user could click
  switch (category) {
    case "bookmark":
      copy = [...user.bookmarks];
      copy = updateGameStatus(copy, game);
      update = { bookmarks: copy };
      break;
    // For the playing case, if we add to it, we want the completed to not have the same game
    case "playing":
      copy = [...user.playing];
      other = [...user.completed];
      i = getCopyIndex(other, game);
      if (i >= 0) {
        other.splice(i, 1);
        update = { completed: other };
        await UserModel.findOneAndUpdate({ email: email }, update);
      }
      copy = updateGameStatus(copy, game);
      update = { playing: copy };
      break;
    // For the completed case, if we add to it, we want the playing to not have the same game
    case "completed":
      copy = [...user.completed];
      other = [...user.playing];
      i = getCopyIndex(other, game);
      if (i >= 0) {
        other.splice(i, 1);
        update = { playing: other };
        await UserModel.findOneAndUpdate({ email: email }, update);
      }
      copy = updateGameStatus(copy, game);
      update = { completed: copy };
      break;
    case "favorite":
      update = { favorite: game };
      if (checkFavorite(user, game)) {
        update = { favorite: null };
      }
      break;
    case "like":
      copy = await updateLikes(user.likes, game);
      update = { likes: copy };
      break;
    default:
      console.log("In default");
      break;
  }
  await UserModel.findOneAndUpdate({ email: email }, update);
}

// This helper function is specifically made to update the like status of a game
// It takes in the array of games that a user has liked, and a new game that has been liked/disliked
// Depending on which of the 2 option it is, a different update will be made on the User object
async function updateLikes(likes, gameId) {
  let copy = [...likes];
  const chosenGame = await GameModel.findOne({ _id: gameId });
  let gameUpdate;
  let i = getCopyIndex(copy, gameId);
  if (i >= 0) {
    copy.splice(i, 1);
    gameUpdate = { likes: --chosenGame.likes };
  } else {
    copy.push(gameId);
    gameUpdate = { likes: ++chosenGame.likes };
  }
  await GameModel.findOneAndUpdate({ _id: gameId }, gameUpdate);
  return copy;
}

// This helper function checks if a game is already inside an array and returns the associated index
function getCopyIndex(copy, game) {
  for (let i = 0; i < copy.length; i++) {
    if (copy[i].toString() === game) {
      return i;
    }
  }
  return -1;
}

// This helper function will either remove or add a game to an array, depending on if it's already in it or not
// The resulting array is sent back
function updateGameStatus(copy, game) {
  let i = getCopyIndex(copy, game);
  if (i >= 0) {
    copy.splice(i, 1);
  } else {
    copy.push(game);
  }
  return copy;
}

// This route will update the games in Playing of a user
/**
 * @swagger
 * /games/{game}/playing:
 *  put:
 *    summary: Modify the 'playing' status of the selected game
 *    description: An update is made on the 'playing' status, by calling helper functions, for the given user and game
 */
router.put("/:game/playing", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// This route will update the games in Completed of a user
/**
 * @swagger
 * /games/{game}/completed:
 *  put:
 *    summary: Modify the 'completed' status of the selected game
 *    description: An update is made on the 'completed' status, by calling helper functions, for the given user and game
 */
router.put("/:game/completed", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// This route will update the favorite game of a user
/**
 * @swagger
 * /games/{game}/favorite:
 *  put:
 *    summary: Modify the 'favorite' status of the selected game
 *    description: An update is made on the 'favorite' status by calling helper functions, for the given user and game
 */
router.put("/:game/favorite", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

// Function checks if the favorite game had already been selected
function checkFavorite(user, game) {
  try {
    if (user.favorite.toString() === game) {
      return true;
    }
  } catch {
    return false;
  }
}

// This route will update the like status of a game for a user
/**
 * @swagger
 * /games/{game}/like:
 *  put:
 *    summary: Modify the 'like' status of the selected game
 *    description: An update is made on the 'like' status by calling helper functions, for the given user and game
 */
router.put("/:game/like", async (req, res) => {
  await updateObj(req.body.category, req.body.email, req.params.game);
  res.sendStatus(202);
});

/**
 * @swagger
 * /games/{game}/image:
 *  get:
 *    summary: Retrieve the image of a given game
 *    description: A query is done to retrieve the game based on id, and then a response of the image is sent back
 */
router.get("/:game/image", async (req, res) => {
  try {
    let game = await GameModel.findOne({ _id: req.params.game });
    res.json({ image: game.img_url });
  } catch (err) {
    // catches casting errors to ObjectId
    res.status(404).send("Invalid game id!");
  }
});

/**
 * @swagger
 * /games/id/{game}:
 *  get:
 *    summary: Retrieve a specific game object based on id
 *    description: An query is made to retrieve a game based on id
 */
router.get("/id/:game", async (req, res) => {
  try {
    let game = await GameModel.findOne({ _id: req.params.game });
    res.json({ game: game });
  } catch {
    res.status(404).send("Invalid game id!");
  }
});

// This route will update the Game object to contain a comment made by a user
router.post("/:id/comment", async (req, res) => {
  //initializing the db models we modify alongside a variable to contain their fields
  let game = await GameModel.findOne({ _id: req.params.id });
  let person = await UserModel.findOne({ email: req.body.email });
  let copyComments = [];
  let copyGames = [];

  // If statement to handle a situation where a game has no comments and its null
  if (game.comments) {
    copyComments = [...game.comments];
  }

  // Update the GameModel with new comment
  copyComments.push({
    user: person._id,
    message: req.body.comment,
  });
  let update = { comments: copyComments };
  await GameModel.findOneAndUpdate({ _id: game }, update);

  // Checks if the user's comments field is null or not
  if (person.comments) {
    copyGames = [...person.comments];
    if (getCopyIndex(copyGames, req.params.id) === -1) {
      copyGames.push(req.params.id);
    }
  } else {
    copyGames.push(req.params.id);
  }

  // Update user model
  update = { comments: copyGames };
  await UserModel.findOneAndUpdate({ email: req.body.email }, update);
  res.sendStatus(202);
});

export { router };
