import React, { useRef, useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import './styles/SearchBar.css';

/**
 * This functional component contains a form to search for games or users by keyword
 * and username respectively. It renders a datalist to show an auto-complete list of results
 * that match the user's input. The form also contains a search button to navigate to the specific game page.
 * @author David Pizzolongo
 * @param placeholder is the text inside the search bar to indicate what to type.
 * @returns search bar
 */
function SearchBar({ placeholder }) {
    // stores newly fetched data from db
    const [data, setData] = useState([]);
    const navigate = useNavigate();
    // contains reference to input element in Virtual DOM
    const inputElem = useRef();
    const isGamePage = placeholder.includes('game');

    // clear datalist array when page changes from games to users 
    useEffect(() => setData([]), [placeholder]);

    // alternate between showing input text as black/red as user enters a game or user 
    useEffect(() => {
        const input = inputElem.current;

        // show input text in black if it is valid (stored in the db) 
        if (data.indexOf(input.value) >= 0) {
            input.style.color = 'black';
        }
        // don't change to red text if user has not typed something already
        else if (inputElem.current.value !== '') {
            input.style.color = 'red';
        }

    }, [inputElem, data]);

    /**
     * This function gets called every time the user enters something new in 
     * the search bar. It calls the filter api to get games/users by keyword. If the response
     * is valid, the data will be extracted from the response and the datalist will be updated. 
     * @author David Pizzolongo
     * @param e represents the element that the function is called upon
     * @returns 
     */
    async function handleFilter(e) {
        const newKeyword = e.target.value;
        try {
            // only fetch if user entered between 1 and 40 characters
            if (newKeyword.length === 0 || newKeyword.length > 40) {
                return;
            }

            if (isGamePage) {
                const response = await fetch('/games/filter/keyword/' + newKeyword)
                if (response.ok) {
                    const filteredGames = await response.json();

                    let uniqueGames = filteredGames.map(game => game.name);
                    // finds unique game titles by making sure that the first index of the game is the current index 
                    uniqueGames = uniqueGames.filter((a, currIndex) => uniqueGames.indexOf(a) === currIndex);
                    setData(uniqueGames);
                }
            }
            else {
                const response = await fetch("/users/name/" + newKeyword);
                if (response.ok) {
                    const users = await response.json();

                    // get usernames from json so datalist can filter by username
                    const usernames = users.map(user => user.username);
                    setData(usernames);
                }
            }
        } catch (error) {
            console.error(error.message);
        }
    }

    /**
     * The following function is called when the user clicks the Search button. 
     * If the game title entered is in the datalist, it is valid and a fetch will occur
     * to get the whole game object. We will then use this as props to navigate to the game's detail page
     * and display all the game's details.
     * @author David Pizzolongo
     * @param e represents the element that the function is called upon
     * @returns 
     */
    async function handleSubmit(e) {
        e.preventDefault();
        const input = inputElem.current.value;
        // input is not in the datalist, so don't fetch
        if (data.indexOf(input) === -1) {
            return;
        }

        try {
            if (isGamePage) {
                // fetch entered game
                const response = await fetch('/games/filter/keyword/' + input);
                if (response.ok) {
                    const game = (await response.json())[0];
                    navigate('game/' + game._id, { state: { game: game } });
                }
            }
            else {
                // fetch entered user
                const response = await fetch('/users/name/' + input);
                if (response.ok) {
                    const user = (await response.json())[0];
                    navigate('users/profile/' + user._id, { state: { email: user.email } })
                }
            }
        }
        catch {
            console.log('Failed to fetch database data!');
        }

        // clear input
        e.target.reset();
    }

    // returns form, containing search bar, search button and datalist elements
    return (
        <form onSubmit={handleSubmit}>
            <input id='searchBar'
                required={true}
                type='search'
                autoComplete='off'
                placeholder={placeholder}
                list='searchData'
                onChange={handleFilter}
                ref={inputElem}
            />
            <datalist id='searchData'>
                {data.map((dataField, index) =>
                    <option key={index} value={dataField} />
                )}
            </datalist>
            <input type='submit' value='Search' id='searchBtn' />
        </form>
    );

}

export default SearchBar;