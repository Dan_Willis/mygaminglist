/* eslint-disable no-undef */
import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";
import logo from "../assets/MyGamingList_Logo.png";
import "./styles/NavBar.css";
import { Link, useLocation } from "react-router-dom";
import SearchBar from "./SearchBar";
import SortForm from "./SortForm";
import GameFilterComponent from "./GameFilterComponent";
/**
 * Display navigation bar with search bar and navigation menu
 * @author Dan Willis
 * @returns navigation bar
 */
function NavBar({ setSortType }) {
  const [user, setUser] = useState({});
  const [id, setId] = useState({});
  // changes when you navigate to different pages
  const url = useLocation();

  useEffect(() => {
    //This function adjusts the image of the current logged in user to match the db and then sets the user
    async function setCurrentUser(userObject) {
      let obj = JSON.parse(userObject);
      try {
        const resp = await fetch(`/users/${obj.email}`);
        let json = await resp.json();
        obj.picture = json.pfp;
        setUser(obj);
        setId(json._id);
      } catch {
        console.log("Failed to set the current logged in user");
      }
    }
    const userObject = localStorage.getItem("loginData");
    if (userObject != null) {
      setCurrentUser(userObject);
    } else {
      try {
        google.accounts.id.initialize({
          client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
          callback: handleCallbackResponse,
        });

        google.accounts.id.renderButton(document.getElementById("signInDiv"), {
          theme: "outline",
          size: "large",
        });
      } catch {
        console.log("Failed to load Google login!");
      }
    }
  }, []);

  async function handleCallbackResponse(response) {
    const userObject = jwt_decode(response.credential);

    // The following block of code is used to check if a user object exists based on login
    let formData = new FormData();
    formData.append(
      "username",
      userObject.given_name + " " + userObject.family_name
    );
    formData.append("profile", userObject.picture);
    formData.append("email", userObject.email);
    const resp = await fetch("/users/login", {
      body: formData,
      method: "post",
    });
    let json = await resp.json();
    userObject.picture = json.profile;
    setUser(userObject);
    document.getElementById("signInDiv").hidden = true;
    localStorage.setItem("loginData", JSON.stringify(userObject));
  }

  function handleSignOut(event) {
    event.preventDefault();
    //remove the user and clear local storage
    setUser({});
    localStorage.removeItem("loginData");
    document.getElementById("signInDiv").hidden = false;
  }

  return (
    <header className="header">
      <div id="topnav">
        <a id="logoLink" href="/">
          <img id="logo" src={logo} alt="logo" />
        </a>
        <SearchBar
          placeholder={
            url.pathname.includes("users")
              ? "Enter a user..."
              : "Enter a game..."
          }
        />

        {/* google authentication */}
        <div id="signInDiv"></div>
        {/* if user is logged in, show sign out button */}
        {Object.keys(user).length !== 0 && (
          <button id="signout" onClick={handleSignOut}>
            Sign Out
          </button>
        )}
        {localStorage.getItem("loginData") && (
          <div>
            <Link to={`users/profile/${id}`} state={{ email: user.email }}>
              <img id="profile-pic" src={user.picture} alt="profile" />
            </Link>
          </div>
        )}
      </div>

      {/* navigation */}
      <div id="bottomnav">
        <div id="navlinks">
          <div id="homeLinkDiv">
            <Link to="/">Home </Link>
          </div>
          <div id="bookmarkLinkDiv">
            <Link to="/users">Users</Link>
          </div>
          <div id="categoryButtonDiv">
            {(url.pathname === "/" || url.pathname.includes("category")) && (
              <GameFilterComponent />
            )}
          </div>
          {/* only show Sort button and dropdown menu if we are on the Home Page */}
          <div id="sortButtonDiv">
            {url.pathname === "/" && (
              <div tabindex="0" id="dropdown">
                <Link to="/" id="sort-btn">
                  Sort &#9662;
                </Link>
                <SortForm setSortType={setSortType} />
              </div>
            )}
          </div>
        </div>

        {/* filters */}
        <select name="filter" id="filter">
          <option value="english">English</option>
          <option value="francais">Français</option>
          <option value="espanol">Español</option>
        </select>
      </div>
    </header>
  );
}

export default NavBar;

