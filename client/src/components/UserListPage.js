import { useEffect, useState } from 'react';
import UserCard from './UserCard';
import Grid2 from '@mui/material/Unstable_Grid2';
import "./styles/UserListPage.css"

/**
 * This component displays all the user cards. If you are an admin user, you can delete other users.
 * @author Christopher Dagenais
 * @returns list of user cards
 */
function UserListPage() {
  const [users, setUsers] = useState([])
  const [admin, setAdmin] = useState(false)

  async function deleteUser(e, email) {
    e.preventDefault()
    await fetch(`/users/delete/${email}`, {
      method: 'DELETE'
    })

    try {
      let response = await fetch("/users/all")
      let result
      if (response.ok) {
        result = await response.json()
        setUsers(result)
      }
    } catch (e) {
      console.error(e.message)
    }
  }

  useEffect(() => {
    async function fetchData() {
      try {
        let response = await fetch("/users/all")
        let result
        if (response.ok) {
          result = await response.json()
          setUsers(result)
        }
      } catch (e) {
        console.error(e.message)
      }
    }
    fetchData()
  }, [users])

  useEffect(() => {
    async function fetchData() {
      try {
        if (JSON.parse(localStorage.getItem("loginData"))?.email != null) {
          let email = JSON.parse(localStorage.getItem("loginData"))?.email
          let response = await fetch(`/users/${email}`)
          let result
          if (response.ok) {
            result = await response.json()
            setAdmin(result.access === 0 ? true : false)
          }
        }

      } catch (e) {
        console.error(e.message)
      }
    }
    fetchData()
  }, [])

  return (
    <div>
      <div id="userContainer">
        <Grid2 container justify="center" spacing={2}>
          {users.map((user) => (
            <Grid2 item xs={6}>
              <UserCard id={user._id} username={user.username} pfp={user.pfp} bio={user.bio} email={user.email} deleteUser={deleteUser} banVisible={admin && (user.email !== JSON.parse(localStorage.getItem("loginData"))?.email) && user.access !== 0} />
            </Grid2>
          ))}
        </Grid2>
      </div>
    </div>
  );
}

export default UserListPage;