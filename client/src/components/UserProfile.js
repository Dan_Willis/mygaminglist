import React from "react";
import { useState, useEffect } from "react";
import GameRow from "./GameRow";
import "./styles/UserProfile.css";
import InfoForm from "./InfoForm";
import { useLocation } from "react-router-dom";
import Banner from "../assets/image-banner.jpg";

/**
 * This component displays a specific user's profile page
 * @author Shiv Patel
 * @returns profile page
 */
function UserProfile() {
  const [favorite, setFavorite] = useState(null);
  const [user, setUser] = useState({});
  const [favoriteText, setFavoriteText] = useState("hi");
  const { state: location } = useLocation();
  const email = location.email;

  // Function that returns a GameRow component if condition is meant
  function createGameRow() {
    if (user.username) {
      return <GameRow user={user} />;
    }
  }

  // Function that returns an InfoForm component if condition is meant
  function createForm() {
    if (user.pfp) {
      return <InfoForm user={user} />;
    }
  }

  // useEffect will set user state when the email of the user changes
  useEffect(() => {
    // This function will fetch all the games and the user object of the current member.
    async function fetchData() {
      try{
        let resp = await fetch(`/users/${email}`);
        let json = await resp.json();
        setUser(json);
      }catch{
        console.log("Failed to fetch user")
      }
    }
    setUser({});
    fetchData();
  }, [email, location]);

  // Use effect that sets favorite game after the games state is actually defined
  useEffect(() => {
    async function setNewFavorite(game) {
      let resp = await fetch(`/games/${game}/image`);
      let json = await resp.json();
      let image = await json.image;
      setFavorite(image);
    }
    // The following if statement is temporary, will be changed once favorite functionality is added
    if (user.favorite != null) {
      setNewFavorite(user.favorite);

      setFavoriteText(
        "You can change your favorite game anytime through the actual game's page."
      );
    } else {
      setFavorite(Banner);
      setFavoriteText(
        "You do not have a favorite game. \n You can change the favorite game through the actual game's page."
      );
    }
  }, [user.favorite]);

  return (
    <div class="flex-container" id="main-container">
      <div class="flex-left">
        <div class="infoBox">{createForm()}</div>
        <div class="faveBox">
          <h2>Favourite Game </h2>
          <img src={favorite} alt="Random thing" class="favorite"></img>
          <p>
            {(!(JSON.parse(localStorage.getItem("loginData"))?.email !== email)) && (
            <small>{favoriteText}</small>
            )}
          </p>
        </div>
      </div>
      <div class="flex-right">{createGameRow()}</div>
    </div>
  );
}

export default UserProfile;
