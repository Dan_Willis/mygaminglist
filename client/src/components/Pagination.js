import "./styles/Pagination.css";
import PaginationItem from "./PaginationItem";

/**
 * Pagination that dynamically changed based on number of elements we want to display.
 * @author MonsterlessonsAcademy
 * Tutorial: https://www.youtube.com/watch?v=jmNHcW_oszg
 * @param { currentPage, total, limit, onPageChange } props make pagination dynamic
 * @returns pagination bar
 */
function Pagination({ currentPage, total, limit, onPageChange }) {
  const range = (start, end) => {
    return [...Array(end - start).keys()].map((el) => el + start);
  };

  // only show limited amount of page number and dynamically changed those numbers based on the current page
  const getPagesCut = ({ pagesCount, pagesCutCount, currentPage }) => {
    const ceiling = Math.ceil(pagesCutCount / 2);
    const floor = Math.floor(pagesCutCount / 2);

    if (pagesCount < pagesCutCount) {
      return { start: 1, end: pagesCount + 1 };
    } else if (currentPage >= 1 && currentPage <= ceiling) {
      return { start: 1, end: pagesCutCount + 1 };
    } else if (currentPage + floor >= pagesCount) {
      return { start: pagesCount - pagesCutCount + 1, end: pagesCount + 1 };
    } else {
      return { start: currentPage - ceiling + 1, end: currentPage + floor + 1 };
    }
  };

  const pagesCount = Math.ceil(total / limit); // do Math.floor to hide the last incomplete page of games
  const pagesCut = getPagesCut({ pagesCount, pagesCutCount: 10, currentPage });
  const pages = range(pagesCut.start, pagesCut.end);
  // boolean to check if it is first page and last page
  const isFirstPage = currentPage === 1;
  const isLastPage = currentPage === pagesCount;
  return (
    <ul className="pagination">
      <PaginationItem
        page="First"
        currentPage={currentPage}
        onPageChange={() => onPageChange(1)}
        isDisabled={isFirstPage}
      />
      <PaginationItem
        page="Prev"
        currentPage={currentPage}
        onPageChange={() => onPageChange(currentPage - 1)}
        isDisabled={isFirstPage}
      />
      {pages.map((page) => (
        <PaginationItem
          page={page}
          key={page}
          currentPage={currentPage}
          onPageChange={onPageChange}
        />
      ))}
      <PaginationItem
        page="Next"
        currentPage={currentPage}
        onPageChange={() => onPageChange(currentPage + 1)}
        isDisabled={isLastPage}
      />
      <PaginationItem
        page="Last"
        currentPage={currentPage}
        onPageChange={() => onPageChange(pagesCount)}
        isDisabled={isLastPage}
      />
    </ul>
  );
}

export default Pagination;
