import GameCard from "./GameCard.js";
import Pagination from "./Pagination.js";
import "./styles/DisplayAllGameCards.css";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

/**
 * Display filtered games. Show 120 games at the time and use pagination to load more games.
 * @author Edris Zoghlami Dan Willis
 * @returns list of games with pagination at the bottom
 */
function DisplayCategoryGameCards() {
  const GAME_LIMIT = 120;
  let { state } = useLocation();
  const [categories, setCategories] = useState("");
  const [games, setGames] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    // get the count of all games in db to calculate pagination
    async function fetchTotalPages() {
      try {
        let tempCategories = "";
        setTotalPages(0);
        for (let i = 0; i < state.length; i++) {
          tempCategories += `${tempCategories}/${state[i].category}`;
        }
        setCategories(tempCategories);
        setCurrentPage(1);
        let response = await fetch(
          `/games/filter/category/count/${categories}`
        );
        let result = await response.json();
        setTotalPages(result.count);
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchTotalPages();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state, categories]);

  // Fetches new subset of games every time the user changes page.
  // If the user clicks on the Sort button, the fetched games will be sorted accordingly.
  useEffect(() => {
    let index = (currentPage - 1) * GAME_LIMIT; //change it for 120 later
    async function fetchData() {
      try {
        let response = await fetch(
          `/games/filter/category/index/${index}${categories}`
        );
        let result;

        result = await response.json();
        setGames(result);
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, categories]);

  window.scrollTo(0, 0);

  return (
    <div>
      <div id="gameContainer">
        {games.map((game) => (
          <GameCard key={game._id} game={game} />
        ))}
      </div>
      <div id="games-pagination">
        <Pagination
          currentPage={currentPage}
          total={totalPages}
          limit={GAME_LIMIT}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </div>
    </div>
  );
}

export default DisplayCategoryGameCards;
