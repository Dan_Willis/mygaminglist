import GameCard from "./GameCard.js";
import Pagination from "./Pagination.js";
import "./styles/DisplayAllGameCards.css";
import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";

/**
 * Display all games. Show 120 games at the time and use pagination to load more games.
 * @author Dan Willis
 * @returns list of games with pagination at the bottom
 */
function DisplayAllGameCards() {
  const GAME_LIMIT = 120;
  const [games, setGames] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);

  const [sortingType] = useOutletContext();

  /**
   * This function sorts an array of games based on the sorting field that the user
   * selected. This could be by rating (number of likes) or title (ascending/descending).
   * It will sort the games in ascending order at first and then reverse the array if needed.
   * @author David Pizzolongo
   * @param {games} group of 120 games unsorted
   * @param {sortField} field used to sort the games (containes the sort order as well)
   * @returns {sortedGames}
   */
  function sortGames(games, sortField) {
    const sortOrder = sortField.includes("Asc") ? "ascending" : "descending";
    // remove sort order from sort field so that the sorting could take place
    sortField = sortField.replace("Asc", "").replace("Desc", "");

    let sortedGames = games.sort((a, b) => {
      // swap games
      if (a[sortField] > b[sortField]) {
        return 1;
      }
      // don't swap games
      else if (a[sortField] < b[sortField]) {
        return -1;
      }
      return 0;
    });

    // if sort order was descending, simply reverse the array
    if (sortOrder === "descending") {
      sortedGames = sortedGames.reverse();
    }

    return sortedGames;
  }

  useEffect(() => {
    // get the count of all games in db to calculate pagination
    async function fetchTotalPages() {
      try {
        let response = await fetch(`/games/count`);
        let result;
        if (response.ok) {
          result = await response.json();
          setTotalPages(result.count);
        }
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchTotalPages();
  }, []);

  // Fetches new subset of games every time the user changes page.
  // If the user clicks on the Sort button, the fetched games will be sorted accordingly.
  useEffect(() => {
    let index = (currentPage - 1) * GAME_LIMIT;
    async function fetchData() {
      try {
        let response = await fetch(`/games/${index}`);
        let result;
        if (response.ok) {
          result = await response.json();
          // sorting type starts as null, until the user chooses a sort option like title
          if (sortingType) {
            result = sortGames(result, sortingType);
          }
          setGames(result); // set updated games
        }
      } catch (error) {
        console.error(error.message);
      }
    }
    fetchData();
  }, [currentPage, sortingType]);

  window.scrollTo(0, 0);

  return (
    <div>
      <div id="gameContainer">
        {games.map((game) => (
          <GameCard key={game._id} game={game} />
        ))}
      </div>
      <div id="games-pagination">
        <Pagination
          currentPage={currentPage}
          total={totalPages}
          limit={GAME_LIMIT}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </div>
    </div>
  );
}

export default DisplayAllGameCards;
