import React from "react";
import { useEffect, useState } from "react";
import "./styles/DisplayComment.css";
import { Link } from "react-router-dom";
/**
 * The DisplayComment component takes as input object representing comments and the user that posted them
 * The data is them displayed using divs and images for the user profile image
 * @author Edris and David
 * @param props contains data like the comment user, the user profile picture and the comment content
 * @returns comment div
 */
function DisplayComment(props) {
  const [user, setUser] = useState({});
  const [isValidUser, setIsValidUser] = useState(true);

  // useEffect will set the user to be the one that's associated to the comment
  useEffect(() => {
    async function fetchUser() {
      try {
        let response = await fetch(`/users/id/${props.userId}`);
        let result;

        if (response.ok) {
          result = await response.json();
          // response was null because there is no user with that id anymore
          if (result == null) {
            setIsValidUser(false);
            return;
          }
          setUser(result);
        }
      } catch (e) {
        console.error(e.message);
      }
    }
    fetchUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div class="comment">
      <div class="comment-content">
        <div class="image-div">
          {/* only show profile pic if user exists */}
          {isValidUser && (
            <Link to="/profile" state={{ email: user.email }}>
              <img class="user-image" src={user.pfp} alt="" />
            </Link>)}
        </div>

        <div class="name-text-comment">
          <div class="username">{isValidUser ? user.username : 'deleted-user'}</div>

          <div class="comment-text">{props.comment}</div>
        </div>

      </div>
    </div>
  );

}

export default DisplayComment;
