import { React, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./styles/GameFilterFormComponent.css";

/**
 * Display the filter category form when the category button is pressed
 * @author Edris Zoghlami
 * @returns the filter category form
 */

// all the categories
const categories = [
  "Action",
  "Adventure",
  "Free",
  "Multiplayer",
  "Singleplayer",

  "Sports",
  "Horror",
  "Shooter",
  "Online",
  "Competitive",

  "Classic",
  "Local",
  "Violent",
  "Fantasy",
  "Arcade",
];

let splitCategories = [];

function GameFilterFormComponent(removeFilterForm) {
  splitCategories = splitCategoryArray();
  const navigate = useNavigate();
  const [categoryState, setCategoryState] = useState(BaseUseState());

  // make all categories false for use state when form is first shown
  function BaseUseState() {
    return categories.map((category) => {
      return { category: category, clicked: false };
    });
  }

  // add or remove a category from the checked category list
  const addRemoveCategory = (category) => {
    let temp = [...categoryState];
    const categoryIndex = temp.findIndex((x) => x.category === category);
    temp[categoryIndex].clicked = !temp[categoryIndex].clicked;
    setCategoryState(temp);
  };

  // handles the submit. Will take only the checked categories and display them
  const handleSubmit = (event) => {
    let checkedCategories = categoryState.filter((allCategories) => {
      return allCategories.clicked === true;
    });
    let categoryString;
    for (let i = 0; i < checkedCategories.length; i++) {
      if (i === 0) {
        categoryString = checkedCategories[i].category;
      } else {
        categoryString = categoryString + "-" + checkedCategories[i].category;
      }
    }
    if (categoryString) {
      navigate(`/category/${categoryString}`, { state: checkedCategories });
    } else {
      navigate(`/`);
    }

    removeFilterForm.removeFilterForm();
    setCategoryState(BaseUseState);
    event.preventDefault();
  };

  //splits the full category list into smaller arrays to display well
  function splitCategoryArray() {
    let splitArray = [];
    for (let i = 0; i < categories.length; i += 5) {
      const chunk = categories.slice(i, i + 5);
      splitArray.push(chunk);
    }
    return splitArray;
  }

  return (
    <div id="game-filter-form-component">
      <form id="filter-form" onSubmit={handleSubmit}>
        <section id="categories">
          {splitCategories.map((categoryArr, index) => {
            let divId = `categoryDiv${index}`;
            return (
              <div id={divId} class="subcategorydiv">
                {categoryArr.map((category) => {
                  return (
                    <label for={category}>
                      <input
                        type="checkbox"
                        id={category}
                        name={category}
                        onChange={() => addRemoveCategory(category)}
                      />
                      {category}
                    </label>
                  );
                })}
              </div>
            );
          })}
        </section>
        <br />
        <input type="submit" id="filterSubmitButton" value="Filter"></input>
      </form>
    </div>
  );
}

export default GameFilterFormComponent;
