import React from 'react'
import { useState, useEffect } from "react";
import './styles/UserProfile.css';
import { Link } from "react-router-dom";
import Banner from "../assets/image-banner.jpg"

/**
 * Creates Game Row component that will display all the games in their respective categories
 * @author Shiv Patel
 * @param props contains user information
 * @returns div containing 3 rows of games
 */
function GameRow(props) {
  const [bookmarks, setBookmark] = useState(props.user.bookmarks);
  const [playing, setPlaying] = useState(props.user.playing);
  const [completed, setCompleted] = useState(props.user.completed);

  // Use effect will set all the state variables
  useEffect(() => {
    // loads default banner before setting fetched banners
    async function setDefault() {
      let bookmarkImg = await Promise.all(bookmarks.map(async (item) => {
        return Banner
      }))

      let playingImg = await Promise.all(playing.map(async (item) => {
        return Banner
      }))
      let completedImg = await Promise.all(completed.map(async (item) => {
        return Banner
      }))

      setBookmark(bookmarkImg)
      setPlaying(playingImg)
      setCompleted(completedImg)
    }

    async function setAllGames() {
      let bookmarkImg = await Promise.all(bookmarks.map(async (item) => {
        let resp = await fetch(`/games/${item}/image`)
        let json = await resp.json();
        let image = await json.image;
        return image
      }))

      let playingImg = await Promise.all(playing.map(async (item) => {
        let resp = await fetch(`/games/${item}/image`)
        let json = await resp.json();
        return json.image
      }))
      let completedImg = await Promise.all(completed.map(async (item) => {
        let resp = await fetch(`/games/${item}/image`)
        let json = await resp.json();
        return json.image
      }))

      setBookmark(bookmarkImg)
      setPlaying(playingImg)
      setCompleted(completedImg)
    }
    setDefault();
    setAllGames();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return <>
    <div><div class="addPad">
      <h3>Bookmarks</h3>
      <Link to="bookmarks" state={{ games: props.user.bookmarks }}>
        <button class="seeAll">See All </button>
      </Link>
    </div>
      <div class="game-container">
        <div class="flex-row">
          {bookmarks.slice(0, 6).map((url, id) => {
            return <img src={url} alt='Game header from Steam' class="gameImage" key={id} />
          })}
        </div>
      </div>
    </div>
    <hr></hr>
    <div>
      <div class="addPad">
        <h3>Playing</h3>
        <Link to="playing" state={{ games: props.user.playing }}>
          <button class="seeAll">See All </button>
        </Link>
      </div><div class="game-container">
        <div class="flex-row">
          {playing.slice(0, 6).map((url, id) => {
            return <img src={url} alt='Game header from Steam' class="gameImage" key={id} />
          })}
        </div>
      </div>
    </div>
    <hr></hr>
    <div>
      <div class="addPad">
        <h3>Completed</h3>
        <Link to="completed" state={{ games: props.user.completed }}>
          <button class="seeAll">See All </button>
        </Link>
      </div>
      <div class="game-container">
        <div class="flex-row">
          {completed.slice(0, 6).map((url, id) => {
            return <img src={url} alt='Game header from Steam' class="gameImage" key={id} />
          })}
        </div>
      </div>
    </div>
  </>
}

export default GameRow
