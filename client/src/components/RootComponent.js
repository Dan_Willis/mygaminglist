import "../App.css";
import NavBar from "./NavBar";
import Footer from "./Footer";
import { Outlet } from "react-router-dom";
import { useState } from "react";

/**
 * Root component that contains the 3 elements that constitute our website (navbar, main, footer)
 * @returns the entire web page
 */
export default function RootLayout() {
  // keeps track of sort type that user enters so we only fetch games when it changes
  const [sortingType, setSortingType] = useState(null);

  return (
    <div className="root-layout">
      <NavBar setSortType={newSortType => setSortingType(newSortType)} />
      <main>
        <Outlet context={[sortingType]} />
      </main>
      <Footer />
    </div>
  );
}
