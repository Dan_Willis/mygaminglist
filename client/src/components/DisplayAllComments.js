import React from "react";
import "./styles/DisplayAllComments.css";
import DisplayComment from "./DisplayComment";
import { FixedSizeList } from "react-window";
import AutoSizer from "react-virtualized-auto-sizer";
import { useLocation } from "react-router-dom";

/**
 * Display all comments for a specific game using lazy loading (prevent from loading all comments in DOM)
 * @author Dan Willis
 * @returns list of comments
 */
function DisplayAllComments() {
  let { state } = useLocation();
  // const [isValidUser, setIsValidUser] = useState(true);
  const commentsArr = state.game.comments || [];

  return (
    // <div id="display-comments-container">
      <div
        id="display-comments-component"
        style={{ flex: "1 1 auto", height: "30vh" }}
      >
        <AutoSizer>
          {({ height, width }) => (
            <FixedSizeList
              height={height * 2}
              itemCount={commentsArr.length}
              itemSize={200}
              width={width}
            >
              {({ index, style }) => (
                <div style={style}>
                  <DisplayComment
                    userId={commentsArr[index].user}
                    comment={commentsArr[index].message}
                  />
                </div>
              )}
            </FixedSizeList>
          )}
        </AutoSizer>
      </div>
    // </div>

  );
}

export default DisplayAllComments;
