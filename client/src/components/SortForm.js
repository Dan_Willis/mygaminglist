import { React, useState } from 'react';
import './styles/SortForm.css';

/**
 * The SortForm component handles the logic of managing the form to sort games.
 * When you choose a sort option and click on the Sort button, the 
 * setSortType function will be called to trigger an 'event' in the parent. 
 * It lets the RootComponent know that the games must be sorted. This component will then
 * tell the DisplayAllGameCards component to show the newly sorted games.
 * @author David Pizzolongo
 * @param setSortType represents the type of sorting selected by the user 
 * @returns radio button form with Sort button 
 */
function SortForm({ setSortType }) {
    // keeps track of user input in the form
    const [oldInput, setOldInput] = useState(null);

    function handleSubmit(e) {
        e.preventDefault();
        const inputs = document.querySelectorAll('.dropdown-input');
        let userInput;
        // get checked input 
        for (const input of inputs) {
            if (input.checked) {
                userInput = input.value;
                break;
            }
        }

        // only update state when input changes from the last sort 
        if (userInput !== oldInput) {
            setOldInput(userInput);
            // update DisplayAllGameCards component 
            setSortType(userInput);
        }
    }

    return (
        <form id='dropdown-form' onSubmit={handleSubmit}>
            <input type='radio' id='rating' name='sort-fields' value='likes' className='dropdown-input' required />
            <label for="rating" className='dropdown-labels'>Rating</label>
            <br></br>
            <input type='radio' id='title-asc' name='sort-fields' value='nameAsc' className='dropdown-input' />
            <label for="title-asc" className='dropdown-labels'>Title ASC</label>
            <br></br>
            <input type='radio' id='title-desc' name='sort-fields' value='nameDesc' className='dropdown-input' />
            <label for="title-desc" className='dropdown-labels'>Title DESC</label>
            <br></br>

            <input type='submit' value='Sort' id='submit-btn' />
        </form>
    );
}

export default SortForm;