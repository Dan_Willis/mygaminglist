import React, { useState, useEffect } from "react";
import { Snackbar } from "@mui/material";
import "./styles/GamePreferenceComponent.css";
import likeButtonSrc from "../assets/likebutton.png";
import favoriteButtonSrc from "../assets/favoritebutton.png";

/**
 * Description .....
 * @author Edris and David
 * @param props contains data such as id, likeCount
 * @returns GamePreferenceComponent div
 */
function GamePreferenceComponent(props) {
  const [logingAlert, setLogingAlert] = useState(false);
  const [like, setLike] = useState(false);
  const [likeCount, setLikeCount] = useState();
  const [favorite, setFavorite] = useState(false);

  // Use effect will set the actual state of each of the buttons
  useEffect(() => {
    // This function will check if the current user has the game on the current page in the given category
    async function verifyIfChecked(preferenceTypes) {
      let userObject = JSON.parse(localStorage.getItem("loginData"));

      if (userObject) {
        let json;
        try{
          const resp = await fetch(`/users/${userObject.email}`);
          json = await resp.json();
        }catch{
          console.log("Failed to fetch user")
          return;
        }
        for (let i = 0; i < preferenceTypes.length; i++) {
          let preferenceType = preferenceTypes[i];
          if (json[preferenceType].includes(props.id)) {
            if (preferenceType === "likes") {
              setLike(true);
              document.querySelector("#like-button").classList.add("liked");
            } else if (preferenceType === "favorite") {
              setFavorite(true);
              document.querySelector("#favorite-button").classList.add("fav");
            } else if (preferenceType === "playing") {
              document.querySelector("#playingBTN").checked = true;
              document.querySelector("#playingLabel").classList.add("checked");
            } else if (preferenceType === "completed") {
              document.querySelector("#completedBTN").checked = true;
              document
                .querySelector("#completedLabel")
                .classList.add("checked");
            } else if (preferenceType === "bookmarks") {
              document.querySelector("#bookmarkBTN").checked = true;
              document.querySelector("#bookmarkLabel").classList.add("checked");
            }
          }
        }
      }
    }
    verifyIfChecked(["likes", "favorite", "playing", "completed", "bookmarks"]);
    setTotalLikes();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Will be called once a render to update the like counter to the most recent database like count
  async function setTotalLikes() {
    try{
      const resp = await fetch(`/games/id/${props.id}`);
      let json = await resp.json();
      setLikeCount(json.game.likes);
    }catch(e){
      console.log("Network error, failed to get number of likes!");
      setLikeCount(NaN);
    }
  }

  // Toggle like will switch the values of everything by calling different functions
  function toggleLike() {
    setLike(!like);
    let likebtn = document.getElementById("like-button");
    if (!like) {
      likebtn.classList.add("liked");
      setLikeCount(likeCount + 1);
    } else {
      likebtn.classList.remove("liked");
      setLikeCount(likeCount - 1);
    }
  }

  //will be called when the favorite button is pressed in order to style it
  function toggleFavorite() {
    setFavorite(!favorite);
    let favoritebtn = document.getElementById("favorite-button");
    if (!favorite) {
      favoritebtn.classList.add("fav");
    } else {
      favoritebtn.classList.remove("fav");
    }
  }

  //global re usable method to post different information to db
  async function postToDB(formData, postType) {
    const resp = await fetch(`/games/${props.id}/${postType}`, {
      body: formData,
      method: "put",
    });
    return resp;
  }

  // This function is called any time the user clicks the like button
  async function likeGame() {
    let userObject = JSON.parse(localStorage.getItem("loginData"));
    if (userObject != null) {
      let formData = new FormData();
      formData.append("email", userObject.email);
      formData.append("category", "like");
      toggleLike();
      // post like to database
      try {
        postToDB(formData, "like");
      } catch {
        console.log("Network error, failed to post like!");
      }
    } else {
      setLogingAlert(true);
    }
  }

  // This function is called any time the user clicks the favorite game button
  async function setFavGame() {
    let userObject = JSON.parse(localStorage.getItem("loginData"));
    if (userObject) {
      let formData = new FormData();
      formData.append("email", userObject.email);
      formData.append("category", "favorite");
      toggleFavorite();
      // change fav game in db
      try {
        postToDB(formData, "favorite");
      } catch {
        console.log("Network error!, failed to make game favorite!");
      }
    } else {
      setLogingAlert(true);
    }
  }

  // This function is called any time the user clicks any of the cataloguing buttons
  async function catalogueGame(catalogueType) {
    let userObject = JSON.parse(localStorage.getItem("loginData"));
    if (userObject) {
      let formData = new FormData();
      formData.append("email", userObject.email);
      formData.append("category", catalogueType);
      try {
        postToDB(formData, catalogueType);

        if (catalogueType === "bookmark") {
          checkInput("bookmarkLabel");
        } else {
          radioInput(`${catalogueType}Label`);
        }
      } catch {
        console.log("Network error!");
      }
    } else {
      setLogingAlert(true);
    }
  }

  //called when the bookmark button is pressed. Depending on the current state, it will either be
  //un colored or colored in
  function checkInput(id) {
    let element = document.getElementById(id);
    if (element.classList.contains("checked")) {
      element.classList.remove("checked");
    } else {
      element.classList.add("checked");
    }
  }
  //called when the completed and playing button is pressed. Depending on the current state, it will either
  //color the playing or completed button
  function radioInput(id) {
    let unChosenElementId = "";
    if (id === "playingLabel") {
      unChosenElementId = "completedLabel";
    } else {
      unChosenElementId = "playingLabel";
    }
    let chosenElement = document.getElementById(id);
    let unChosenElement = document.getElementById(unChosenElementId);
    if (chosenElement.classList.contains("checked")) {
      chosenElement.classList.remove("checked");
      unChosenElement.classList.add("checked");
    } else {
      chosenElement.classList.add("checked");
      unChosenElement.classList.remove("checked");
    }
  }

  return (
    <>
      <div id="classify-game-div">
        <div id="game-score-div">
          <div id="game-score-sub-div">
            <div id="game-score">Score: {likeCount}</div>
          </div>
          <div id="like-game-div">
            <img
              id="like-button"
              src={likeButtonSrc}
              onClick={likeGame}
              alt="likebutton"
              title="Like"
            ></img>
          </div>

          <div id="favorite-game-div">
            <img
              id="favorite-button"
              src={favoriteButtonSrc}
              onClick={setFavGame}
              alt="likebutton"
              title="Favorite"
            ></img>
          </div>
        </div>

        <div id="user-catalogue-div">
          <label
            class="bookmarkBTNLabel"
            id="bookmarkLabel"
            for="bookmarkBTN"
            title="Bookmark"
          >
            <input
              type="checkbox"
              name="input-option"
              id="bookmarkBTN"
              class="preferenceInput"
              onChange={() => {
                catalogueGame("bookmark");
              }}
            />
            Bookmark
          </label>

          <label
            class="catalogueBTNLabel"
            id="playingLabel"
            for="playingBTN"
            title="Playing"
          >
            <input
              type="radio"
              name="input-option"
              id="playingBTN"
              class="preferenceInput"
              onChange={() => {
                catalogueGame("playing");
              }}
            />
            Playing
          </label>

          <label
            class="catalogueBTNLabel"
            id="completedLabel"
            for="completedBTN"
            title="Completed"
          >
            <input
              type="radio"
              name="input-option"
              id="completedBTN"
              class="preferenceInput"
              onChange={() => {
                catalogueGame("completed");
              }}
            />
            Completed
          </label>
        </div>

        <Snackbar
          message="You must be logged in to use this feature"
          autoHideDuration={4000}
          open={logingAlert}
          onClose={() => setLogingAlert(false)}
        />
      </div>
    </>
  );
}

export default GamePreferenceComponent;
