import React from 'react'
import { useState } from 'react'
import "./styles/InfoForm.css"

/**
 * Form component used to update and set user data on web page
 * @author Shiv Patel
 * @param user contains user information like bio, username, pfp
 * @returns form for modifying profile info when clicking Edit button
 */
function InfoForm({ user }) {
  const [bio, setBio] = useState(user.bio)
  const [name, setName] = useState(user.username)
  const [email] = useState(user.email)
  const [profile, setProfile] = useState(user.pfp)
  const [edit, setEdit] = useState(false)
  const [pfpEdit, setPfpEdit] = useState(false)

  // Helper function that details what to do on submit 
  async function handleSubmit(e) {
    e.preventDefault();

    // Initializing the data to be sent to the api
    let formData = new FormData();
    formData.append('email', email)
    formData.append('bio', bio)
    formData.append('name', name)
    if (pfpEdit) {
      formData.append('file', profile);
      setPfpEdit(!pfpEdit)
    }
    else {
      formData.append('profile', profile)
    }

    setEdit(!edit);
    try{
      const resp = await fetch("/users/edit",
        {
          body: formData,
          method: "post"
        });
      let json = await resp.json()
      setProfile(json.pfp)
    }catch{
      console.log("Failed to edit the user profile")
    }
  }

  // Helper function that sets the value of the name state
  function nameHandler(e) {
    setName(e.target.value);
  }

  // Helper function that sets the value of the bio state
  function bioHandler(e) {
    setBio(e.target.value);
  }

  // Helper function that sets the value of the profile state
  function fileSelectedHandler(e) {
    // Add db and api calls to change files into blob storage url
    setProfile(e.target.files[0])
    setPfpEdit(!pfpEdit)
  }

  // If user is trying to edit the page, we display a form, otherwise, we display a static component
  if (edit) {
    return <form onSubmit={handleSubmit} class="form">
      <fieldset>
        <label> Change your name:
          <input type="text" name="username" value={name} onChange={nameHandler} minlength="3" maxlength="30" required></input>
        </label>

        <label> Change your Bio:
          <input type="text" name="bio" value={bio} onChange={bioHandler} minlength="1" maxlength="65"></input>
        </label>

        <label> This is your email:
          <input type="email" name="email" value={email} readonly></input>
        </label>

        <label for="profile"> Change Profile Picture:
          <input type="file" name="profile" id="profile" accept="image/*" onChange={fileSelectedHandler} />
        </label>


        <button type="submit">Submit</button>
      </fieldset>
    </form>
  }
  return <>
    <h2> Profile Page </h2>
    <div>
      <img src={profile} alt='User Profile' class="pfp" />
    </div>
    <div>
      <p>{name}<br />{bio}<br /></p><br />
      {(!(JSON.parse(localStorage.getItem("loginData"))?.email !== email)) && (
        <button onClick={() => { setEdit(!edit) }} id="edit_button"> Edit </button>
      )}
    </div>
  </>
}

export default InfoForm