import React, { useState, useEffect, useRef } from "react";
import GameFilterFormComponent from "./GameFilterFormComponent";
import "./styles/GameFilterComponent.css";

/**
 * Button that when pressed, it displays the category form
 * @author Edris Zoghlami
 * @returns the show filter form button
 */

function GameFilterComponent() {
  const [showForm, setFilterForm] = useState(false);
  const ref = useRef(null);

  //event handlers to show and hide category form. Will hide after off click or when button is re pressed or form
  //submitted
  const toggleFilterForm = () => {
    setFilterForm(!showForm);
  };

  const removeFilterForm = () => {
    setFilterForm(false);
  };

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      removeFilterForm();
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  });

  return (
    <div ref={ref} id="game-filter-component">
      <button id="game-filter-button" onClick={toggleFilterForm}>
        Categories &#9662;
      </button>

      {showForm && (
        <div id="game-filter-form">
          <GameFilterFormComponent removeFilterForm={removeFilterForm} />
        </div>
      )}
    </div>
  );
}

export default GameFilterComponent;
