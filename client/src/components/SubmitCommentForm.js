import React from "react";
import { useState } from "react";
import "./styles/SubmitCommentForm.css";
import { Snackbar } from "@mui/material";
import arrow from "../assets/arrow.png"

/**
 * This component allows a user to send comments for a specific game
 * @author Edris Zoghlami
 * @param props contains game_id
 * @returns form for sending comments
 */
function SubmitCommentForm(props) {
  const [comment, setComment] = useState("");
  const [logingAlert, setLogingAlert] = useState(false);

  function commentHandler(e) {
    setComment(e.target.value);
  }

  // Helper function that details what to do on submit
  async function handleSubmit(e) {
    e.preventDefault();
    let userObject = JSON.parse(localStorage.getItem("loginData"));
    if (userObject) {
      let formData = new FormData();
      formData.append("email", userObject.email);
      formData.append("comment", comment);

      const resp = await fetch(`/games/${props.id}/comment`, {
        body: formData,
        method: "post",
      });
      if (resp.ok) {
        console.log("Comment was posted");
      }
    } else {
      setLogingAlert(true);
    }
    e.target.reset();
  }

  return (
    <>
    <div id="submit-comment-container">
      <div id="submit-comment-component">
          <h2 id="leave-comment-h2">Share a Comment</h2>

          <form id="comment-form" onSubmit={handleSubmit}>
            <textarea
              form="comment-form"
              id="comment-textarea"
              name="comment"
              placeholder="Type here"
              rows="3"
              onChange={commentHandler}
              required
            ></textarea>

            <button id="submit-comment-btn"><img id="send-img" src={arrow} alt="Send"/></button>
          </form>

          <Snackbar
            message="You must be logged in to submit a comment"
            autoHideDuration={4000}
            open={logingAlert}
            onClose={() => setLogingAlert(false)}
          />
        </div>
    </div>
      
    </>
  );
}

export default SubmitCommentForm;
