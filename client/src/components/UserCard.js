import { Link } from "react-router-dom";
import "./styles/UserCard.css"

/**
 * This component represent an individual user card containing the user pfp, username and bio.
 * @author Christopher Dagenais
 * @param props contains user properties such as id, username, pfp, id, etc
 * @returns a single user card div
 */
function UserCard(props) {

  return (
    <div className="user-card">
      <Link to={`profile/${props.id}`} state={{ email: props.email }}>
        <img className="user-pfp" src={props.pfp} alt="profile avatar"></img>
      </Link>

      <div className="user-name-and-bio">
        <div className="user-name-container">
          {props.username}
          <button className="user-ban-button" onClick={(e) => { props.deleteUser(e, props.email) }} style={{ visibility: props.banVisible ? 'visible' : 'hidden' }}>Delete</button>
        </div>
        <div className="user-bio-container">
          <p className="user-bio-text">{props.bio}</p>
        </div>
      </div>
    </div>
  );
}

export default UserCard;