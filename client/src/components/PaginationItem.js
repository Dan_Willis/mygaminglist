import classNames from "classnames"

/**
 * Pagination item that change page and get active when selected
 * @author MonsterlessonsAcademy 
 * Tutorial: https://www.youtube.com/watch?v=jmNHcW_oszg
 * @param { page, currentPage, onPageChange, isDisabled } props make pagination item dynamic
 * @returns pagination item
 */
function PaginationItem({ page, currentPage, onPageChange, isDisabled }) {
  const liClasses = classNames({
    "page-item": true,
    active: page === currentPage,
    disabled: isDisabled,
  });

  function checkKeyDown(e) {
    if (e.keyCode === 13) {
      onPageChange(page)
    }
  }

  return (
    <li tabindex="0" className={liClasses} onClick={() => onPageChange(page)} onKeyDown={(e) => checkKeyDown(e)}>
      <span className="page-link">{page}</span>
    </li>
  );
};

export default PaginationItem