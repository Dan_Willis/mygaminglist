import { React } from "react";
import "./styles/GameCard.css";
import Tooltip from "./Tooltip";
import Banner from "../assets/image-banner.jpg";
import "./styles/Tooltip.css";
import "./styles/GameCard.css";
import { Link } from "react-router-dom";
import { LazyLoadImage } from "react-lazy-load-image-component";

/**
 * The GameCard component returns a div container for each game. It contains
 * a clickable game image, a Tooltip and a header with the game's title.
 * @author David Pizzolongo, Dan Willis
 * @param game prop that contains data like its title and description
 * @returns GameCard div
 */
function GameCard({ game }) {
  return (
    <div className="gameCard">
      <Link to={"/game/" + game._id} state={{ game: game }} title="Learn more!">
        <LazyLoadImage
          src={game.img_url}
          alt="Game header from Steam"
          effect="blur"
          placeholderSrc={Banner}
          className="gameImg"
        />
        <Tooltip gameTitle={game.name} gameDesc={game.desc} />
      </Link>

      <h3 className="gameTitle">{game.name}</h3>
    </div>
  );
}

export default GameCard;
