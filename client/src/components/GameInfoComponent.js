import React from "react";
import "./styles/GameInfoComponent.css";


/**
 * The GameInfoComponent takes as input a props containing all the necessary information
 * about the chosen game's information relating to production. It displays them all using many divs and 
 * returns 1 game-info div representing them all.
 * @author Edris Zoghlami
 * @param game contains data like release date, developer, publisher and categories
 * @returns GameInfoComponent div
 */
function GameInfoComponent(game) {
  return (

    <div id="game-info">
      <div id="release-date-div">
        <div class="info-subtitle">Release Date:</div>
        <div class="info-content">{game.date}</div>
      </div>

      <div id="developer-div">
        <div class="info-subtitle">Developer:</div>
        <div class="info-content">{game.developer}</div>
      </div>

      <div id="publisher-div">
        <div class="info-subtitle">Publisher:</div>
        <div class="info-content">{game.publisher}</div>
      </div>

      <div id="categories-div">
        <div class="info-subtitle">Categories:</div>
        <div class="info-content">
          {game.tags.map((tag) => (
            <span>{tag}, </span>
          ))}
        </div>
      </div>
    </div>
  );
}

export default GameInfoComponent;
