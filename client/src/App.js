import "./App.css";

import React from "react";
import DisplayGameDetails from "./components/DisplayGameDetails";
import DisplayAllGameCards from "./components/DisplayAllGameCards";
import DisplayCategoryGameCards from "./components/DisplayCategoryGameCards";
import RootComponent from "./components/RootComponent";
import {
  createRoutesFromElements,
  RouterProvider,
  Route,
  createBrowserRouter,
} from "react-router-dom";
import UserProfile from "./components/UserProfile";
import UserListPage from "./components/UserListPage";
import DisplayProfileGames from "./components/DisplayProfileGames";

// Browser router that allows navigation between views from different components
const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<RootComponent />}>
      <Route index element={<DisplayAllGameCards />} />
      <Route path="/" element={<DisplayAllGameCards />} />
      <Route path="game/:id" element={<DisplayGameDetails />} />
      <Route path="profile" element={<UserProfile />} />
      <Route path="profile/bookmarks" element={<DisplayProfileGames />} />
      <Route path="profile/playing" element={<DisplayProfileGames />} />
      <Route path="profile/completed" element={<DisplayProfileGames />} />
      <Route path="users/profile/:id" element={<UserProfile />} />
      <Route path="users" element={<UserListPage />} />
      <Route
        path="category/:categories"
        element={<DisplayCategoryGameCards />}
      />
      <Route
        path="users/profile/:id/bookmarks"
        element={<DisplayProfileGames />}
      />
      <Route
        path="users/profile/:id/playing"
        element={<DisplayProfileGames />}
      />
      <Route
        path="users/profile/:id/completed"
        element={<DisplayProfileGames />}
      />
    </Route>
  )
);

function App() {
  return (
    <div className="App" data-testid="App-test">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
