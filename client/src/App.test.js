import React from "react";
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders div container for the home page', () => {
  render(<App />);

  const div = screen.getByTestId('App-test');
  expect(div.childNodes.length).toBe(1);
  expect(div.childNodes[0].nodeName).toBe('DIV');
});
